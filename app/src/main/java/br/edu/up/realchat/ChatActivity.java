package br.edu.up.realchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    private Button btnEnviar;
    private EditText txtMensagem;
    private TextView txtChat;

    private String mensagem, usuario;

    private String sala;
    private DatabaseReference root;
    private String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        btnEnviar = (Button) findViewById(R.id.button);
        txtMensagem = (EditText) findViewById(R.id.editText);
        txtChat = (TextView) findViewById(R.id.textView);
        usuario = getIntent().getExtras().get("usuario").toString();
        sala = getIntent().getExtras().get("sala").toString();
        setTitle("Room - " + sala);

        root = FirebaseDatabase.getInstance().getReference().child(sala);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String, Object> map = new HashMap<>();
                key = root.push().getKey();
                root.updateChildren(map);

                DatabaseReference child = root.child(key);
                Map<String, Object> dados = new HashMap<>();
                dados.put("usuario", usuario);
                dados.put("mensagem", txtMensagem.getText().toString());

                child.updateChildren(dados);

            }
        });

        root.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                atualizarConversa(dataSnapshot);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                atualizarConversa(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void atualizarConversa(DataSnapshot dataSnapshot) {
        Iterator iterator = dataSnapshot.getChildren().iterator();
        while (iterator.hasNext()) {
            DataSnapshot ds1 = (DataSnapshot) iterator.next();
            usuario = (String) ds1.getValue();
            DataSnapshot ds2 = (DataSnapshot) iterator.next();
            mensagem = (String) ds2.getValue();
            txtChat.append(usuario + " : " + mensagem + "\n");
        }
    }
}